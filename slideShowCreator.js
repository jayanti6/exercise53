function SlideShowCreator(domElements){
  this.slideshow = domElements.slideshow;
  this.body = domElements.body;
  this.listItems = this.slideshow.find(domElements.itemToFind);
  this.slideshowInterval = domElements.slideshowInterval;
}

SlideShowCreator.prototype.moveSlideShowToTop = function(){
  this.body.prepend(this.slideshow);
  this.createNavigationArea();
}

SlideShowCreator.prototype.createNavigationArea = function(){
  this.navigationArea = $("<div></div>");
  this.slideshow.after(this.navigationArea);
  this.navigationArea.text("There are " + this.listItems.length + " images. You are viewing ");
  this.imageNumberArea = $("<strong></strong>");
  this.navigationArea.append(this.imageNumberArea);
}

SlideShowCreator.prototype.cycleListItems = function(listItem){
  this.imageNumberArea.text(listItem.prevAll().length + 1 + " image");

  listItem.fadeIn(this.slideshowInterval).delay(this.slideshowInterval).fadeOut(this.slideshowInterval, $.proxy(function(){
    if(listItem.next().length == 0){
      this.cycleListItems(this.listItems.first());
    } else {
      this.cycleListItems(listItem.next());
    }
  }, this));
}

SlideShowCreator.prototype.startSlideShow = function(){
  this.listItems.hide();
  this.cycleListItems(this.listItems.first())
}

SlideShowCreator.prototype.init = function(){
  this.moveSlideShowToTop();
  this.startSlideShow();
};

$(document).ready(function(){
  var domElements = {
    slideshow: $("#slideshow"),
    body: $("body"),
    itemToFind: "li",
    slideshowInterval: 1000
  }
  var slideshowCreator = new SlideShowCreator(domElements);
  slideshowCreator.init();
});
